package jp.alhinc.katsumi_misato.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;



public class CalculateSales {
	public static void main(String[] args) {
     	HashMap<String, String> mapBranch = new HashMap<String, String>();
     	HashMap<String, Long> mapSales = new HashMap<String, Long>();
     	BufferedReader br =null;
     	try {
    		File branch = new File(args[0],"branch.lst");
    		if (!branch.exists()) {
    			System.out.println("支店定義ファイルが存在しません");
    			return;
    		}
    		FileReader fileReader = new FileReader(branch);
    		br = new BufferedReader(fileReader);
    		String branchline ;
    		while ((branchline = br.readLine()) != null) {
    			String[] branchdata = branchline.split("\\,");
    			if (!branchdata[0].matches("^[0-9]{3}$")) {
    				System.out.println("支店定義ファイルのフォーマットが不正です");
    				return;
    			}
    			if (branchdata.length != 2) {
    				System.out.println("支店定義ファイルのフォーマットが不正です");
    				return;
    			}
    			mapBranch.put(branchdata[0], branchdata[1]);
    			mapSales.put(branchdata[0], (long) 0);
    		}
     	} catch (IOException e) {
    		System.out.println("予期せぬエラーが発生しました");
    		return;
    	} finally {
    		if (br != null) {
    			try {
    				br.close();
    			} catch (IOException e) {
    				System.out.println("予期せぬエラーが発生しました");
    				return;
    			}
    		}
    	}
        File file = new File(args[0]);
     	File[] salesfile = file.listFiles();
     	ArrayList<Integer> filelist = new ArrayList<Integer>();
     	for (int i = 0 ; i < salesfile.length ; i++) {
     		if(salesfile[i].isFile() && salesfile[i].getName().matches("^[0-9]{8}\\.rcd$")) {
     			String filename[] = salesfile[i].getName().split("\\.");
     			filelist.add(Integer.parseInt(filename[0]));
     			List<String> salesData = new ArrayList<>();
     			try {
     				FileReader fileReader = new FileReader(salesfile[i]);
     				br = new BufferedReader(fileReader);
     				String salesdata;
     				while ((salesdata = br.readLine()) != null) {
     				salesData.add(salesdata);
     				}
     				if (!(mapSales.containsKey(salesData.get(0)))) {
     					System.out.println(salesfile[i].getName() + "の支店コードが不正です");
    			   	  	return;
      			    }
     				long salessum = mapSales.get(salesData.get(0)) + Long.parseLong(salesData.get(1));
     				if (String.valueOf(salessum).length() > 10) {
     					System.out.println("合計金額が10桁を超えました");
     					return;
     				}
     				mapSales.put(salesData.get(0), salessum);
     				int salesline = salesData.size();
     				if (salesline != 2) {
     					System.out.println(salesfile[i].getName() + "のフォーマットが不正です");
     					return;
     				}
     			} catch (IOException e) {
     				System.out.println("予期せぬエラーが発生しました");
     			} finally {
     				if (br != null) {
     					try {
     						br.close();
     					} catch (IOException e) {
     						System.out.println("予期せぬエラーが発生しました");
     						return;
     					}
     				}
     			}
     		}
     	}
     	for (int u = 0; u < filelist.size() - 1; u++) {
    		if (filelist.get(u + 1) - filelist.get(u) != 1 ) {
    			System.out.println("売上ファイル名が連番になっていません");
    			return;
    		}
     	}
     	BufferedWriter bw = null;
     	try {
     		File outfile = new File(args[0],"branch.out");
     		FileWriter fw = new FileWriter(outfile);
     		bw = new BufferedWriter(fw);
     		PrintWriter pw = new PrintWriter(bw);
     		for(String key : mapBranch.keySet()) {
     			pw.println(key + "," + mapBranch.get(key) + "," + mapSales.get(key));
            }
     	} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
     	} finally {
     		try {
     			bw.close();
     		} catch (IOException e) {
     			System.out.println("予期せぬエラーが発生しました");
     			return;
     		}
     	}
	}
}

